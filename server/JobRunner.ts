///////////////////////////////////////////////////////////////////////////////
// Copyright (C) 2019 AcceleratXR, Inc. All rights reserved.
///////////////////////////////////////////////////////////////////////////////
import { Job } from "./Job";

/**
 * The current status of a job runner.
 *
 * @author Jean-Philippe Steinmetz <info@acceleratxr.com>
 */
export interface JobRunnerStatus {
    uid: string;
    jobs: Array<Job>;
    threads: number;
}

/**
 * The `JobRunner` is responsible for executing a given `Job`.
 *
 * @author Jean-Philippe Steinmetz <info@acceleratxr.com>
 */
export interface JobRunner {
    /** The unique identifier of the runner. */
    uid(): string;

    /**
     * Notifies the runner to queue the given job to be immediately processed.
     *
     * @param job The job to begin processing.
     */
    queue(job: Job): Promise<void>;

    /**
     * Notifies the runner to begin waiting for jobs.
     */
    start(): Promise<void>;

    /**
     * Stops the job with the given unique identifier.
     * @param id The unique identifier of the job to stop.
     */
    stop(id: string): Promise<void>;

    /**
     * Notifies the runner to stop running all active jobs.
     */
    shutdown(): Promise<void>;

    /**
     * Returns the current status of the runner.
     */
    status(): JobRunnerStatus;
}
