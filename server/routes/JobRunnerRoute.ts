///////////////////////////////////////////////////////////////////////////////
// Copyright (C) 2019 AcceleratXR, Inc. All rights reserved.
///////////////////////////////////////////////////////////////////////////////
import { Param, Put, Route, Delete } from "@acceleratxr/services_common";
import { JobRunnerStatus } from "../JobRunner";
import JobCoordinator from "../JobCoordinator";

@Route("/runners/:id")
export default class JobRunnerRoute {
    /**
     * Updates the job runner.
     */
    @Put()
    private update(@Param("id") id: string, status: JobRunnerStatus): void {
        JobCoordinator.updateRunnerStatus(id, status);
    }

    /**
     * Unregisters the job runner from the cluster.
     */
    @Delete()
    private delete(@Param("id") id: string): void {
        if (JobCoordinator.runners.has(id)) {
            JobCoordinator.unregister(JobCoordinator.runners.get(id));
        }
    }
}
