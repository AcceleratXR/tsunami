///////////////////////////////////////////////////////////////////////////////
// Copyright (C) 2019 AcceleratXR, Inc. All rights reserved.
///////////////////////////////////////////////////////////////////////////////
import { Route, Get, Response } from "@acceleratxr/services_common";
import { Response as XResponse } from "express";
import * as path from "path";

@Route("/app")
export default class AppRoute {
    @Get()
    private get(@Response res: XResponse): void {
        res.sendFile(path.resolve("./build/index.html"));
    }
}
