///////////////////////////////////////////////////////////////////////////////
// Copyright (C) 2019 AcceleratXR, Inc. All rights reserved.
///////////////////////////////////////////////////////////////////////////////
import { Route, Param, Put, Delete } from "@acceleratxr/services_common";
import { JobStatus } from "../Job";
import JobCoordinator from "../JobCoordinator";

@Route("/jobs/:id")
export default class JobRoute {
    /**
     * Submits a new job to be executed by one or more available runners.
     */
    @Put()
    private update(@Param("id") id: string, status: JobStatus): void {
        JobCoordinator.updateJobStatus(id, status);
    }

    @Delete()
    private delete(@Param("id") id: string): void {
        JobCoordinator.stop(id);
    }
}
