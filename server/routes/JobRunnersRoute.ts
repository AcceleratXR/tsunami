///////////////////////////////////////////////////////////////////////////////
// Copyright (C) 2019 AcceleratXR, Inc. All rights reserved.
///////////////////////////////////////////////////////////////////////////////
import { Route, Get, Post, Param } from "@acceleratxr/services_common";
import { JobRunnerStatus } from "../JobRunner";
import JobCoordinator from "../JobCoordinator";
import { Job } from "../Job";

@Route("/runners")
export default class JobRunnersRoute {
    /**
     * Registers a new job runner with this coordinator.
     */
    @Post()
    private create(obj: any): void {
        const job: Job = new Job(obj);
        JobCoordinator.queue(job);
    }

    /**
     * Returns all job runners managed by this coordinator.
     */
    @Get()
    private get(): Array<JobRunnerStatus> {
        const results: Array<JobRunnerStatus> = new Array();
        for (const status of JobCoordinator.runnerStatus.values()) {
            results.push(status);
        }
        return results;
    }
}
