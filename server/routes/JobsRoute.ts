///////////////////////////////////////////////////////////////////////////////
// Copyright (C) 2019 AcceleratXR, Inc. All rights reserved.
///////////////////////////////////////////////////////////////////////////////
import { Route, Post, Get } from "@acceleratxr/services_common";
import { Job, JobStatus } from "../Job";
import JobCoordinator from "../JobCoordinator";

@Route("/jobs")
export default class JobsRoute {
    /**
     * Submits a new job to be executed by one or more available runners.
     */
    @Post()
    private create(job: Job): void {
        job = new Job(job);
        JobCoordinator.queue(job);
    }

    @Get()
    private get(): Array<JobStatus> {
        const results: Array<JobStatus> = new Array();
        for (const status of JobCoordinator.jobStatus.values()) {
            results.push(status);
        }
        return results;
    }
}
