///////////////////////////////////////////////////////////////////////////////
// Copyright (C) 2019 AcceleratXR, Inc. All rights reserved.
///////////////////////////////////////////////////////////////////////////////
import { ThreadPool } from "@acceleratxr/utilities";
const uuid = require("uuid");

/**
 * The current runtime status of a job.
 *
 * @author Jean-Philippe Steinmetz <info@acceleratxr.com>
 */
export class JobStatus {
    /** The unique identifier of the job. */
    public uid: string;
    /** The name of the job. */
    public name: string;
    /** The number of execution successes that have occurred. */
    public successes: number = 0;
    /** The number of execution failures that have occured. */
    public failures: number = 0;
    /** The number of virtual users that have been executed. */
    public vus: number;
    /** The current state of the job. Possible values are `FAILED`, `RUNNING`, `COMPLETE`. */
    public state: string;
    /** The last error that occurred while executing the job. */
    public error: any;

    constructor(other: any = {}) {
        this.uid = other && other.uid ? other.uid : this.uid;
        this.name = other && other.name ? other.name : this.name;
        this.successes = other && other.successes ? other.successes : this.successes;
        this.failures = other && other.failures ? other.failures : this.failures;
        this.vus = other && other.vus ? other.vus : this.vus;
        this.state = other && other.state ? other.state : this.state;
        this.error = other & other.error ? other.error : this.error;
    }
}

/**
 * A `Job` is a queueable piece of work that will be executed by an available `JobRunner` instance.
 *
 * @author Jean-Philippe Steinmetz <info@acceleratxr.com>
 */
export class Job {
    /** The universally unique identifier of the job. */
    public readonly uid: string = uuid.v4();
    /** The name of the job. */
    public readonly name: string = undefined;
    /** The contents of the script to run. */
    public readonly script: string = undefined;
    /** The total amount of time to execute the job in each runner. */
    public readonly duration: string = undefined;
    /** The number of concurrent executions of the job to run in each runner. */
    public readonly vus: number = 1;
    /** The machine time, in milliseconds, that the job began execution. */
    public startTime: number;
    /** The list of runtime arguments to pass to the script. */
    public args: string[] = [];

    /**
     * Creates a new instance of `Job` using the provided defaults.
     *
     * @param other The other object whose properties will be copied.
     */
    constructor(other: any) {
        this.uid = other && other.uid ? other.uid : this.uid;
        this.name = other && other.name ? other.name : this.name;
        this.script = other ? other.script : this.script;
        this.duration = other ? other.duration : this.duration;
        this.vus = other ? other.vus : this.vus;
        this.startTime = other && other.startTime ? other.startTime : this.startTime;
        this.args = other && other.args ? other.args : this.args;

        if (!this.script) {
            throw new Error("Object must specify the script property.");
        }
    }
}
