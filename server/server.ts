#!/usr/bin/env node
///////////////////////////////////////////////////////////////////////////////
// Copyright (C) 2019 AcceleratXR, Inc. All rights reserved.
///////////////////////////////////////////////////////////////////////////////
import config from "./config";
import { Server } from "@acceleratxr/services_common";
import { Logger } from "@acceleratxr/service_utilities";
import JobCoordinator from "./JobCoordinator";

import * as process from "process";

const logger = Logger();

// Parse the CLI options
let server: Server | undefined = undefined;

const start = async function(config: any, logger: any) {
    await JobCoordinator.init(logger, true);
    // Create and start the AcceleratXR™ server
    server = new Server(config, undefined, "./dist", logger);
    await server.start();
};

start(config, logger);

process.on("SIGINT", async () => {
    logger.info("Shutting down...");
    JobCoordinator.shutdown();
    if (server) {
        await server.stop();
    }
    process.exit(0);
});
