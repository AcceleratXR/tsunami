import React from "react";
import "./App.css";
import Layout from "./components/Layout";
import Editor from "./pages/Editor";

const App: React.FC = () => {
    return (
        <Layout>
            <div className="Content">
                <Editor />
            </div>
        </Layout>
    );
};

export default App;
