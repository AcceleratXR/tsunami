import React from "react";
const { ModalBody, ModalFooter, FormGroup, Label, Input, Button } = require("reactstrap");

interface Props extends React.Props<StartTestModal> {
    onConfirm: Function;
    onCancel: Function;
}

interface State {
    vus: number;
    duration: string;
}

const initialState = {
    vus: 1,
    duration: "1m",
};

class StartTestModal extends React.Component<Props, State> {
    constructor(props: any) {
        super(props);

        this.onCancel = this.onCancel.bind(this);
        this.onChangeInput = this.onChangeInput.bind(this);
        this.onSubmit = this.onSubmit.bind(this);

        this.state = initialState;
    }

    onChangeInput(e: any) {
        e.preventDefault();
        this.setState({
            ...this.state,
            [e.target.id]: e.target.value,
        });
    }

    onCancel(event: Event): void {
        event.preventDefault();
        this.props.onCancel();
    }

    onSubmit(event: Event): void {
        event.preventDefault();
        this.props.onConfirm(this.state.vus, this.state.duration);
    }

    render(): any {
        return (
            <div>
                <ModalBody>
                    <FormGroup>
                        <Label>Virtual Users</Label>&nbsp;
                        <Input defaultValue={this.state.vus} id="vus" onChange={this.onChangeInput} />
                        <br />
                        <Label>Duration</Label>&nbsp;
                        <Input defaultValue={this.state.duration} id="duration" onChange={this.onChangeInput} />
                    </FormGroup>
                </ModalBody>
                <ModalFooter>
                    <Button onClick={this.onSubmit}>Run</Button>
                    <Button onClick={this.onCancel}>Cancel</Button>
                </ModalFooter>
            </div>
        );
    }
}

export default StartTestModal;
