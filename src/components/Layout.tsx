import React from "react";
import Sidebar from "./Sidebar";
import Footer from "./Footer";

export default class Layout extends React.Component {
    render(): any {
        return (
            <div className="App">
                <Sidebar />
                <div className="App-content">{this.props.children}</div>
                <Footer />
            </div>
        );
    }
}
