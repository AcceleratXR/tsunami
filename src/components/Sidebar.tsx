import React from "react";
import { IoIosDocument, IoMdAnalytics } from "react-icons/io";

export default class Sidebar extends React.Component {
    render(): any {
        return (
            <div className="App-sidebar">
                <div>
                    <IoIosDocument className="Sidebar-button" />
                    <br />
                    <IoMdAnalytics className="Sidebar-button" />
                </div>
            </div>
        );
    }
}
