import React from "react";
import Dropzone from "react-dropzone";
const { Card, CardBody, FormGroup, Label, Input, Button } = require("reactstrap");

const initialState = {
    files: [],
    vus: 1,
    duration: "1m",
};

export default class LoadFile extends React.Component {
    readonly state: any = initialState;

    constructor(props: any) {
        super(props);

        this.onDrop = this.onDrop.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    }

    onDrop(files: File[]): void {
        this.setState({
            ...this.state,
            files,
        });
    }

    onSubmit(event: Event): void {
        event.preventDefault();
    }

    render(): any {
        return (
            <Card>
                <CardBody>
                    <p>Welcome to Tsunami!</p>
                    <Dropzone onDrop={this.onDrop}>
                        {({ getRootProps, getInputProps }) => (
                            <section>
                                <div {...getRootProps()}>
                                    <input {...getInputProps()} />
                                    <p>Drag 'n' drop some files here, or click to select files</p>
                                </div>
                            </section>
                        )}
                    </Dropzone>
                    <FormGroup>
                        <Label>Virtual Users</Label>&nbsp;
                        <Input defaultValue={this.state.vus} />
                        <br />
                        <Label>Duration</Label>&nbsp;
                        <Input defaultValue={this.state.duration} />
                        <br />
                        <Button onClick={this.onSubmit}>Run</Button>
                    </FormGroup>
                </CardBody>
            </Card>
        );
    }
}
