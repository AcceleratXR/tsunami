import React from "react";

export default class Footer extends React.Component {
    render(): any {
        return (
            <div className="Footer">
                Copyright &copy; 2019{" "}
                <a href="https://acceleratxr.com" target="_blank" rel="noopener noreferrer">
                    AcceleratXR, Inc.
                </a>{" "}
                All rights reserved.
            </div>
        );
    }
}
