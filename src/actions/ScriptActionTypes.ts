export enum ScriptActionTypes {
    CLOSE_SCRIPT = "CLOSE_SCRIPT",
    LOAD_SCRIPT = "LOAD_SCRIPT",
    NEW_SCRIPT = "NEW_SCRIPT",
    SAVE_SCRIPT = "SAVE_SCRIPT",
}
