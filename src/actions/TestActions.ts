import { TestActionTypes } from "./TestActionTypes";
import axios from "axios";

const Actions = {
    getTestStatus() {
        return async function(dispatch: Function) {
            try {
                const jobs: any[] = await axios.get("http://localhost:3080/jobs");
                dispatch({
                    type: TestActionTypes.GET_STATUS,
                    results: jobs,
                });
            } catch (error) {
                dispatch({
                    type: TestActionTypes.GET_STATUS,
                    error,
                });
            }
        };
    },

    runTest(name: string, script: string, vus: number, duration: string) {
        return async function(dispatch: Function) {
            try {
                await axios.post("http://localhost:3080/jobs", { name, script, vus, duration });
                dispatch({
                    type: TestActionTypes.RUN_TEST,
                    name,
                });
            } catch (error) {
                dispatch({
                    type: TestActionTypes.RUN_TEST,
                    name,
                    error,
                });
            }
        };
    },

    stopTest(id: string) {
        return async function(dispatch: Function) {
            try {
                await axios.delete("http://localhost:3080/jobs/" + id);
                dispatch({
                    type: TestActionTypes.STOP_TEST,
                    id,
                });
            } catch (error) {
                dispatch({
                    type: TestActionTypes.STOP_TEST,
                    id,
                    error,
                });
            }
        };
    },
};

export default Actions;
