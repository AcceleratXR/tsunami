export enum TestActionTypes {
    RUN_TEST = "RUN_TEST",
    STOP_TEST = "STOP_TEST",
    GET_STATUS = "GET_TEST_STATUS",
}
