# tsunami

[![pipeline status](https://gitlab.com/AcceleratXR/tsunami/badges/master/pipeline.svg)](https://gitlab.com/AcceleratXR/tsunami/commits/master)
[![coverage report](https://gitlab.com/AcceleratXR/tsunami/badges/master/coverage.svg)](https://gitlab.com/AcceleratXR/tsunami/commits/master)

A clustered and easily scriptable load testing tool.

With Tsunami you can write load tests using the tools you already know and love, NodeJS, and run them in a clustered, multi-threaded environment to simulate thousands of simulatenous virtual users.

## Installation

Installing Tsunami is simple using npm or yarn.

### NPM

```
npm install -g tsunami
```

### Yarn

```
yarn global add tsunami
```

## Command Line

Tsunami can be run with either a user interface or as a headless command line utility.

### Usage

```
Usage: tsunami [OPTIONS]

A clustered and easily scriptable load testing tool.

OPTIONS:
                -i --input      The test script to run.
                -d --duration   The length of time to perform the test (e.g. 10s, 1m, 1h)
                -u --vus        The number of virtual users to simulate.
                -m --master     The host address of the master coordinator.
                                Set this to have the local instance run as a worker node in a cluster.
                -s --headless   Runs the tool in headless mode with CLI output only.
                                Note that an input file and vus must be specified to run in headless mode.
                -v --version    Prints the version of the tool.
                -h --help       Displays this help dialog.

EXAMPLES:

                tsunami
                        Starts a local master coordinator with a web UI.

                tsunami -s -i mytest.ts -u 1000
                        Runs the test file mytest.ts in CLI mode with 1000 simulated users.

                tsunami -s -i mytest.ts -u 1000 -d 10m
                        Runs the test file mytest.ts in CLI mode with 1000 simulated users for a continuous duration of 10m.

                tsunami -m http://master:3080
                        Starts the tool as a remote job runner and connects to the master coordinator at the specified address.
```
